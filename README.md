# Jenkins Vagrant Machine

Create a Centos/7 virtual machine with Jenkins installed.

## Getting Started

These instructions will get you up and running on your local machine for
development and testing purposes.

### Prerequisites

Install Virtualbox https://www.virtualbox.org/ 

Install Vagrant https://www.vagrantup.com/

Install git https://gitforwindows.org/

Clone the repository onto your local machine

```
git clone https://bitbucket.org/oboudry/vagrantjenkins.git
cd vagrantjenkins
```

Provision the virtual machine

```
vagrant up
```

Copy initial admin password that will be output at the end of the process,
and use it to logon to Jenkins on http://192.168.33.10:8080/

Once you're finished playing with the VM, you can either suspend (can be restarted with `vagrant up`) it or destroy
it.

```
vagrant suspend
vagrant destroy
```

## Authors

* **Olivier Boudry** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
